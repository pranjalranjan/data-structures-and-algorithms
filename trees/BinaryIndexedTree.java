import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BinaryIndexedTree
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        //Number of elements
        int n = Integer.parseInt(bufferedReader.readLine());
        String line = bufferedReader.readLine();
        String[] strs = line.split("\\s+");

        int[] arr = new int[n+1];

        for (int i = 1; i <= n; i++)
        {
            arr[i] = Integer.parseInt(strs[i-1]);
        }

        int[] binaryIndexedTree = new int[n+1];

        for (int i = 1 ; i <= n; i++)
        {
            binaryIndexedTree[i] = 0;
        }

        int q = Integer.parseInt(bufferedReader.readLine());

        for (int i = 1; i<=n; i++)
        {
            updateIndex(binaryIndexedTree, i, arr[i], n);
        }

        while(q > 0)
        {
            q--;
            line = bufferedReader.readLine();
            strs = line.split("\\s+");

            if(strs[0].equalsIgnoreCase("u"))
            {
                int index = Integer.parseInt(strs[1]);
                int toAdd = Integer.parseInt(strs[2]);
                updateIndex(binaryIndexedTree, index, toAdd, n);
            }
            else
            {
                int leftIndex = Integer.parseInt(strs[1]);
                int rightIndex = Integer.parseInt(strs[2]);

                System.out.println(getSumTillIndex(binaryIndexedTree, rightIndex) - getSumTillIndex(binaryIndexedTree, leftIndex - 1));
            }
        }
    }

    private static int getSumTillIndex(int[] bit , int index)
    {
        int sum = 0;

        for (; index > 0; index -= index & (-index))
        {
            sum += bit[index];
        }
        return sum;
    }

    private static void updateIndex(int[] bit, int index, int toAdd, int n)
    {
        for (; index <= n; index += index & (-index))
        {
            bit[index] += toAdd;
        }
    }
}
